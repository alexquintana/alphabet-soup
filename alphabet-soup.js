/*
 * Alphabet Soup Enlighten Exercise
 * by Alex Quintana <alex@ilovephones.net>
 *
 * This requires Node.js >= 11 and is ran from
 * the command line.
 *
 * Usage: node alphabet-soup.js [<config file>]
 *
 */

// we need this to be able to access the filesystem
const fs = require('fs').promises;

// Easy readable directions (going clockwise)
// except right and left because we can use native JS functions for that
const up = { x: 0, y: -1};
const upRight = { x: 1, y: -1};
const downRight = { x: 1, y: 1};
const down = { x: 0, y: 1};
const downLeft = { x: -1, y: 1};
const leftUp = { x: -1, y: -1};

// The directions that aren't as simple as a native function
const circular = [up, upRight, downRight, down, downLeft, leftUp];

// Initialize variables
let gridSizeX;
let gridSizeY;
let grid = [];
let words = [];
let configFile = null;

// add function to String class to reverse the string
String.prototype.reverse = function() {
	return this.split('').reverse().join('');
}

// read and parse config from a file
async function readConfig(file) {
	// read config file into array
	let cfg = null;

	// gracefully exit if there's any problem reading the config
	try {
		cfg = await fs.readFile(file, 'ascii');
	} catch (e) {
		console.log(`Error reading config file: ${file}`);
		process.exit(1);
	}

	// convert to an array for parsing
	cfg = cfg.split(/\r?\n/);

	// set grid size
	[gridSizeX, gridSizeY] = cfg[0].split('x');
	gridSizeX = parseInt(gridSizeX);
	gridSizeY = parseInt(gridSizeY);

	// read the grid
	for (let i = 1; i <= gridSizeY; i++) {
		// add grid row,
		// get rid of all whitespace and ensure case insensitivity
		// I know you said not to do error handling but this is easy peasie and makes me sleep beter at night :)
		grid.push(cfg[i].replace(/\s/g, '').toUpperCase());
	}

	// read the words
	for (let i = gridSizeY + 1; i < cfg.length; i++) {
		if (cfg[i] !== '') {
			words.push(cfg[i].toUpperCase());
		}
	}
}

// check to see if a certain x/y coordinate is out of bounds of the grid
function indexOutOfBounds(x, y) {
	let result = false;

	if (x < 0 || x > gridSizeX - 1) {
		result = true;
	} else if (y < 0 || y > gridSizeY - 1) {
		result = true;
	}

	return result;
}

function findWords() {
	// find the words
	for (let wordIndex = 0; wordIndex < words.length; wordIndex++) {
		let word = words[wordIndex].replace(/\s/g, '');
		let startLetter = word.charAt(0);
		let endLetter = word.charAt(word.length-1);
		let startIndex = null;
		let endIndex = null;
		let foundWord = false;

		for (let rowIndex = 0; rowIndex < grid.length; rowIndex++) {
			let row = grid[rowIndex];

			// check left to right
			if (row.indexOf(word) !== -1) {
				let start = index = row.indexOf(word);
				let end = index + (word.length - 1);
				console.log(`${words[wordIndex]} ${rowIndex}:${start} ${rowIndex}:${end}`);
				break;

			// check right to left
			} else if (row.indexOf(word.reverse()) !== -1) {
				let reversedWord = word.reverse();
				let end = index = row.indexOf(reversedWord);
				let start = index + (word.length - 1);
				console.log(`${words[wordIndex]} ${rowIndex}:${start} ${rowIndex}:${end}`);
				break;

			// check other directions		
			} else {
				for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
					// start on second index since by the time we need this we already know the first letter matches
					let wordLetterIndex = 1;
					// calling this letter instead of cell because it better represents what it is
					let columnLetter = row.charAt(columnIndex);

					// check to see if the word starts in this column
					if (columnLetter === startLetter) {
						// set the start index
						let startIndex = `${rowIndex}:${columnIndex}`;

						// go through the directions listed at the top
						for (let directionIndex = 0; directionIndex < circular.length; directionIndex++) {
							// set the direction we're going in for each axis
							let directionX = circular[directionIndex].x;
							let directionY = circular[directionIndex].y;

							// calculate next grid cell
							let currentX = columnIndex + directionX;
							let currentY = rowIndex + directionY;

							// keep checking letters one by one in our direction until we go out of bounds
							while (!indexOutOfBounds(currentX, currentY)) {
								// get the next letter in the word
								let wordNextLetter = word.charAt(wordLetterIndex);
								// get the next letter in the column
								let nextGridLetter = grid[currentY].charAt(currentX);

								// this isn't the word, move on to next direction
								if (wordNextLetter !== nextGridLetter) {
									break;
								// since we're here, the letter matches. let's see if it's the last letter
								} else if (wordLetterIndex === word.length - 1) {
									// it is! we found the word!
									foundWord = true;
									// set the end index
									endIndex = `${currentY}:${currentX}`;
									break;
								}

								// calculate next grid cell
								currentX += directionX;
								currentY += directionY;

								// up the word letter index by one so we can get the word's next letter above
								wordLetterIndex++;		
							}

							// if we found the word, print the output and move on
							// no need to search other directions
							if (foundWord) {
								console.log(`${words[wordIndex]} ${startIndex} ${endIndex}`);
								break;
							}
						}
					}
					// since we found the word already no need look for it in other columns
					if (foundWord) {
						break;
					}					
				}
			}
			// since we found the word already no need look for it in other rows
			if (foundWord) {
				break;
			}					
		}
	}
}

async function main() {
	// process command line arguments or use default config if none
	configFile = (process.argv.length > 2) ? process.argv[2] : 'soup.cfg';

	// read the config and wait for the results
	await readConfig(configFile);

	// find the words and print the output
	findWords();
}

// Run main function
main();

// aeq/2k20
